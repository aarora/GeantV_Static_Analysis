// This is a simple AST Visitor Program to visit all the Call Expressions in the program, i.e., visit all the function calls and print out their name.

#include "clang/Driver/Options.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/ASTConsumers.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "clang/AST/Type.h"
#include <string>
#include <iostream>

using namespace std;
using namespace clang;
using namespace clang::driver;
using namespace clang::tooling;
using namespace llvm;


int numExprs = 0 ;
static cl::OptionCategory ClangCheckCategory("checkMaskedAssign options");

class myASTVisitor : public RecursiveASTVisitor<myASTVisitor> {
private:
    ASTContext *astContext; //used for getting additional AST info
    
public:
    explicit myASTVisitor(CompilerInstance *CI) : astContext(&(CI->getASTContext())) {
        
    }
    virtual ~myASTVisitor(){
    }
    
    // function to visit all the call expressions, jump on to their function declarations and then get their info from there.
    virtual bool VisitCallExpr(CallExpr *callexpr) {
        FunctionDecl * func_decl = callexpr->getDirectCallee();
        DeclarationNameInfo name_info= func_decl->getNameInfo();
        std::string func_name = name_info.getAsString();
        errs()<<"\n"<<func_name<<"\n";
        SourceLocation loc = callexpr->getExprLoc();
        loc.dump(astContext->getSourceManager());
        return true;
    }
};

class myASTConsumer : public ASTConsumer {
private:
    myASTVisitor *visitor;
    
public:
    explicit myASTConsumer(CompilerInstance *CI) : visitor(new myASTVisitor(CI)) {
        
    } //override the constructor to pass CI
    
    //override the call so our AST Visitor can visit the entire source file
    virtual void HandleTranslationUnit(ASTContext &Context) {
        //get TranslationUnitDecl - Collectively entire source file
        visitor->TraverseDecl(Context.getTranslationUnitDecl());
    }
};

class myFrontendAction : public ASTFrontendAction {
public:
    virtual std::unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &CI, StringRef file) {
        return std::unique_ptr<ASTConsumer>(new myASTConsumer(&CI));
    }
};



int main(int argc, const char **argv) {
    // parse the command line arguments passed to your code
    CommonOptionsParser op(argc,argv,ClangCheckCategory);
    // create a new ClangTool instance (a LibTooling Environment)
    ClangTool Tool(op.getCompilations(), op.getSourcePathList());
    
    // run the Clang Tool, creating a new FrontendAction
    int result = Tool.run(newFrontendActionFactory<myFrontendAction>().get());
    
    errs() << "\nFound "<<numExprs<<" Exprs.\n\n";
    
    
    return result;
}