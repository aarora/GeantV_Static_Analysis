// History - Created 			
// 16th November, 2016         
// Anmol Arora


Please clone this repository in :

$LLVM_SOURCE_DIR/tools/clang/tools/GeantV_Static_Analysis


Also, add the following line to $LLVM_SOURCE_DIR/tools/clang/tools/CMakeLists.txt :
add_subdirectory(GeantV_Static_Analysis)

Now, go to the build directory and to use the latest version of tools, type :
make clang install

After this step, you should be able to use the latest version of tools.

Please use "Finding Bugs Using Clang Tools.pdf" for more details on the background of these tools and also to learn to design more such tools.

Please note that all of these tools can be used as a base for learning about Clang AST or write more tools.


i. checkif

This tools traverses all the if statements in the program, checks if their conditions has variables which are not POD Type and also not dependent type and indicates error with the location.


ii. checkif_vc_mask

This tool traverses all the if statements in the program and checks if the variable inside the if statement is actually a Vc::Mask Variable type casted into a boolean.


iii. print_func_name 

This is a simple AST Visitor Program to visit all the Call Expressions in the program, i.e., visit all the function calls and print out their names. This tool is only in the repository to be used as a starting point to create other tools.

iv. checkMaskedAssign

This is a simple AST Visitor Program to visit all the MaskedAssign Call Expressions in the program and analysing if the first argument supplied as an argument to is always a bool. If it's always a bool, then indicate an error. 
A better version of it is implemented using an AST Matcher. Check the other tool "checkMA" for better version of the tool.

v. checkMA

This is a better version of checkMaskedAssign. Implemented as a very simple example of AST Matcher, this is supposed to analyse the arguments of a MaskedAssign function and point out an error when the first argument is always a bool.
At this moment, this tool has a lot of false positives. This tool requires some further discussions and modifications to be fully functional and produce meaningful results.

vi. cast_counter

This is an interesting AST Matcher Class Implementation to detect all the casts happening in the program and print them out on the screen.
Like  print_function_name, this tool is only supposed to be a starting point for creation of a new tool.

vii. checkcast_visitor_vc_mask_bool

This is an AST Visitor Program to visit all the casts in the program and check for the presence of a cast from Vc::Mask to a Boolean.

viii. checkcast_matcher_vc_mask_bool

This is a better AST Matcher Implementation of the similar visitor program. It detects all the implicit type casts from Vc::Mask to a boolean, even if they're in a template-type instantiations (which AST Visitor is skipping somehow). The only thing not implemented in this code is the filtering out of header files from the reports , which is added to the next tool.

ix. checkcast_matcher_vc_bool_ignr_dir

This is the final version of the tool which also filters out bugs from a specific source directory, which helps in making more meaningful reports. See Comments in the Source File for Usage.



