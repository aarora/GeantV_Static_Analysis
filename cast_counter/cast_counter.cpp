// This is a program to detect all the casts happening in the program and print them out on the screen.

// It is a very interesting and standard implementation of AST Matcher Class
// Like  print_function_name, this tool is only supposed to be a starting point for creation of a new tool.

#include "clang/Driver/Options.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/ASTConsumers.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "clang/AST/Type.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include <string>
#include <iostream>

using namespace std;
using namespace clang;
using namespace clang::driver;
using namespace clang::tooling;
using namespace clang::ast_matchers;
using namespace llvm;


int numExprs = 0 ;
static cl::OptionCategory ClangCheckCategory("checkcast_matcher options");


class ImplicitCastAnalyser : public MatchFinder::MatchCallback {
public:
    virtual void run(const MatchFinder::MatchResult &Result) {
        
        if(const CastExpr *cast_expr = Result.Nodes.getNodeAs<clang::CastExpr>("castexpr"))
        {
            numExprs++;
            string booltype = "_Bool";
            QualType q1 = cast_expr->getType();
            string type1 = q1.getAsString();
            
            const Expr *expr2 = cast_expr->IgnoreImpCasts();
            QualType q2 = expr2->getType();
            string type2 = q2.getAsString();
            
            const Expr *expr3 = expr2->IgnoreConversionOperator();
            QualType q3 = expr3->getType();
            string type3 = q3.getAsString();
            
            const Expr *expr4 = cast_expr->getSubExprAsWritten();
            QualType q4 = expr4->getType();
            string type4 = q4.getAsString();
            
                errs()<<"\n"<<type4<<"-->"<<type3<<"-->"<<type2<<"-->"<<type1;
                errs()<<"\n";
                SourceLocation loc = cast_expr->getExprLoc();
                loc.dump(*(Result.SourceManager));
                errs() << ": Found a cast here";

        }
    }
};



class myASTConsumer : public ASTConsumer {
private:
    ImplicitCastAnalyser Analyser;
    MatchFinder Finder;
    
public:
    myASTConsumer(CompilerInstance *CI) {
        Finder.addMatcher(castExpr().bind("castexpr"), &Analyser);
    }
    

    virtual void HandleTranslationUnit(ASTContext &Context) {
        //get TranslationUnitDecl - Collectively entire source file
        Finder.matchAST(Context);
    }
};


class MyFrontendAction : public ASTFrontendAction {
public:
    MyFrontendAction() {}
    
    std::unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &CI, StringRef file) {
        return std::unique_ptr<ASTConsumer>(new myASTConsumer(&CI));
    }
    
    
    
};

int main(int argc, const char **argv) {
    // parse the command line arguments passed to your code
    CommonOptionsParser op(argc,argv,ClangCheckCategory);
    // create a new ClangTool instance (a LibTooling Environment)
    ClangTool Tool(op.getCompilations(), op.getSourcePathList());
    
    // run the Clang Tool, creating a new FrontendAction
    int result = Tool.run(newFrontendActionFactory<MyFrontendAction>().get());
    
    errs() << "\nFound "<<numExprs<<" Exprs.\n\n";
    
    return result;
}
