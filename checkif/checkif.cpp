// This is a simple AST Visitor Program to Visit all the If Statements in a program,
// Jump off to their conditions and check them if they're not a POD Type or Dependent Type.


// Comments
// <dependent type> is used in the implementation because apparently the clang AST Function
// to detect the Dependent Type wasn't working as expected.
// You might need to check if it's fixed in the latest implementation and is working as expected.



#include "clang/Driver/Options.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/ASTConsumers.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "clang/AST/Type.h"
#include <string>
#include <iostream>


// All the Namespaces.
using namespace std;
using namespace clang;
using namespace clang::driver;
using namespace clang::tooling;
using namespace llvm;

// Global Counter for Total Number of Ifs detected in the program.
int numIfs = 0 ;

static cl::OptionCategory ClangCheckCategory("checkif options");


// Our AST Visitor which actually does most of the job
class myASTVisitor : public RecursiveASTVisitor<myASTVisitor> {
private:
    ASTContext *astContext; //used for getting additional AST info
    
public:
    explicit myASTVisitor(CompilerInstance *CI) : astContext(&(CI->getASTContext())) {
        
    }
    virtual ~myASTVisitor() {
    }
    
    //Our function to visit al the if statements and perform some operations on them.
    virtual bool VisitIfStmt(IfStmt *if_stmt) {
            numIfs++;
            Expr* e;
            e = if_stmt->getCond();
            e = e->IgnoreImpCasts();
            e = e->IgnoreConversionOperator();
            
            QualType q;
            q = e->getType();
            string type = q.getAsString();
    
            if(q.isPODType(*astContext) && type!="<dependent type>")
            {
                // Output Messages which were previously used for observing some behaviours and testing.
                /*
                SourceLocation loc = if_stmt->getIfLoc();
                errs() << "\nThis one is Okay : ";
                loc.dump(astContext->getSourceManager());
                 */
            }
            else
            {
                errs()<<"\n\n"<<type;
                SourceLocation loc = if_stmt->getIfLoc();
                errs() << "\nFound problem : ";
                loc.dump(astContext->getSourceManager());
            }
        return true;
    }
};



// Our AST Consumer which will actually call our Visitor Program and perform the necessary actions.
class myASTConsumer : public ASTConsumer {
private:
    myASTVisitor *visitor;
    
public:
    explicit myASTConsumer(CompilerInstance *CI) : visitor(new myASTVisitor(CI)) {
        
    } //override the constructor to pass CI
    
    //override the call so our AST Visitor can visit the entire source file
    virtual void HandleTranslationUnit(ASTContext &Context) {
        //get TranslationUnitDecl - Collectively entire source file
        visitor->TraverseDecl(Context.getTranslationUnitDecl());
    }
};

// Front End Actions to run over the code and call our AST Consumer, which will in turn, call our AST Visitor program.
class myFrontendAction : public ASTFrontendAction {
public:
    virtual std::unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &CI, StringRef file) {
        return std::unique_ptr<ASTConsumer>(new myASTConsumer(&CI));
    }
};



int main(int argc, const char **argv) {
    // parse the command line arguments passed to your code
    CommonOptionsParser op(argc,argv,ClangCheckCategory);
    // create a new ClangTool instance (a LibTooling Environment)
    ClangTool Tool(op.getCompilations(), op.getSourcePathList());
    
    // run the Clang Tool, creating a new FrontendAction
    int result = Tool.run(newFrontendActionFactory<myFrontendAction>().get());
    
    errs() << "\nFound "<<numIfs<<" Ifs.\n\n";
    
    
    return result;
}