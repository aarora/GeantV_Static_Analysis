// This tool is a simple example of AST Matcher implementation.


// Comments :- At this moment, this tool has a lot of false positives. This tool requires some further discussions and modifications to be fully functional and produce meaningful results.

#include "clang/Driver/Options.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/ASTConsumers.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "clang/AST/Type.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include <string>
#include <iostream>

using namespace std;
using namespace clang;
using namespace clang::driver;
using namespace clang::tooling;
using namespace clang::ast_matchers;
using namespace llvm;

int numMaskedAssign = 0 ;
static cl::OptionCategory ClangCheckCategory("checkMaskedAssign options");

StatementMatcher MaskedAssignMatcher = callExpr( callee( functionDecl(hasName("MaskedAssign")))).bind("callexpr");

class MaskedAssignAnalyser : public MatchFinder::MatchCallback {
public:
    virtual void run(const MatchFinder::MatchResult &Result) {
        if(const CallExpr *callexpr = Result.Nodes.getNodeAs<clang::CallExpr>("callexpr"))
        {
            
                    const clang::Expr *const * args = callexpr->getArgs();
                    const clang::Expr *const arg1 = args[0];
                    const Expr* argument1 = arg1->IgnoreImpCasts();
                    const Expr* argument1_1 = argument1->IgnoreConversionOperator();
                        
                    QualType q;
                    q  = argument1_1->getType();
                
                        string booltype = "_Bool";
                        string type = q.getAsString();

                    
                        if(type==booltype)
                        {
                            numMaskedAssign++;
                            errs()<<type<<"\n";
                            SourceLocation loc = callexpr->getExprLoc();
                            string location = loc.printToString(*(Result.SourceManager));
                            errs()<<location<<" : Found the first Argument to be Boolean Type\n";
                            
                        }
            /*
                        else
                        {
                            errs()<<"\nAt Least the first argument isn't Boolean Type\n";
                        }
             */
        }
    }
};


int main(int argc, const char **argv) {
    // parse the command line arguments passed to your code
    CommonOptionsParser op(argc,argv,ClangCheckCategory);
    // create a new ClangTool instance (a LibTooling Environment)
    ClangTool Tool(op.getCompilations(), op.getSourcePathList());
    
    MaskedAssignAnalyser Analyser;
    MatchFinder Finder;
    Finder.addMatcher(MaskedAssignMatcher, &Analyser);
    
    
    // run the Clang Tool, creating a new FrontendAction
    int result = Tool.run(newFrontendActionFactory(&Finder).get());
    
    errs() << "\nFound "<<numMaskedAssign<<" Occurences.\n\n";
    
    return result;
}