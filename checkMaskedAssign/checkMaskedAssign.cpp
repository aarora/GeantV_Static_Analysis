// This is a simple AST Visitor Program to visit all the MaskedAssign Call Expressions in the program and analysing if the first argument supplied as an argument to is always a bool. If it's always a bool, then indicate an error.

// Comments : A better version of this is already implemented. This is just for reference purpose now. Check the other tool "checkMA" for a better version of this tool.

#include "clang/Driver/Options.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/ASTConsumers.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "clang/AST/Type.h"
#include <string>
#include <iostream>

using namespace std;
using namespace clang;
using namespace clang::driver;
using namespace clang::tooling;
using namespace llvm;


int numMaskedAssign = 0 ;
static cl::OptionCategory ClangCheckCategory("checkMaskedAssign options");

class myASTVisitor : public RecursiveASTVisitor<myASTVisitor> {
private:
    ASTContext *astContext; //used for getting additional AST info
    
public:
    explicit myASTVisitor(CompilerInstance *CI) : astContext(&(CI->getASTContext())) {
        
    }
    virtual ~myASTVisitor(){
    }
    virtual bool VisitCallExpr(CallExpr *callexpr) {
        if(astContext->getSourceManager().isInMainFile(callexpr->getExprLoc()))
        {
            const FunctionDecl * func_decl = callexpr->getDirectCallee();
            if(func_decl->hasBody(func_decl))
            {
                DeclarationNameInfo name_info= func_decl->getNameInfo();
                std::string func_name = name_info.getAsString();
                std::string MaskedAssign("MaskedAssign");
                if (func_name==MaskedAssign) {
                    numMaskedAssign++;
                    errs()<<"\n"<<func_name<<"\n";
                    SourceLocation loc = callexpr->getExprLoc();
                    loc.dump(astContext->getSourceManager());
                    Expr** args = callexpr->getArgs();
                    Expr* arg1 = args[0];
                    arg1 = arg1->IgnoreImpCasts();
                    arg1 = arg1->IgnoreConversionOperator();
                
                    QualType q;
                    q  = arg1->getType();
                    string booltype = "_Bool";
                    string type = q.getAsString();
                    errs()<<"\n"<<type<<"\n";
                
                    if(type==booltype)
                    {
                        errs()<<"\nFound the first Argument to be Boolean Type\n";
                    }
                    else
                    {
                        errs()<<"\nAt Least the first argument isn't Boolean Type\n";
                    }
                }
            }
        }
        return true;
    }
};

class myASTConsumer : public ASTConsumer {
private:
    myASTVisitor *visitor;
    
public:
    explicit myASTConsumer(CompilerInstance *CI) : visitor(new myASTVisitor(CI)) {
        
    } //override the constructor to pass CI
    
    //override the call so our AST Visitor can visit the entire source file
    virtual void HandleTranslationUnit(ASTContext &Context) {
        //get TranslationUnitDecl - Collectively entire source file
        visitor->TraverseDecl(Context.getTranslationUnitDecl());
    }
};

class myFrontendAction : public ASTFrontendAction {
public:
    virtual std::unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &CI, StringRef file) {
        return std::unique_ptr<ASTConsumer>(new myASTConsumer(&CI));
    }
};



int main(int argc, const char **argv) {
    // parse the command line arguments passed to your code
    CommonOptionsParser op(argc,argv,ClangCheckCategory);
    // create a new ClangTool instance (a LibTooling Environment)
    ClangTool Tool(op.getCompilations(), op.getSourcePathList());
    
    // run the Clang Tool, creating a new FrontendAction
    int result = Tool.run(newFrontendActionFactory<myFrontendAction>().get());
    
    errs() << "\nFound "<<numMaskedAssign<<" Occurences.\n\n";
    
    
    return result;
}