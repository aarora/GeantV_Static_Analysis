// This is an AST Visitor Program to visit all the casts in the program and check for the presence of a cast from Vc::Mask to a Boolean.

#include "clang/Driver/Options.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/ASTConsumers.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "clang/AST/Type.h"
#include <string>
#include <iostream>

using namespace std;
using namespace clang;
using namespace clang::driver;
using namespace clang::tooling;
using namespace llvm;


int numExprs = 0 ;
static cl::OptionCategory ClangCheckCategory("myfirsttool options");

class myASTVisitor : public RecursiveASTVisitor<myASTVisitor> {
private:
    ASTContext *astContext; //used for getting additional AST info
    
public:
    explicit myASTVisitor(CompilerInstance *CI) : astContext(&(CI->getASTContext())) {
        
    }
    virtual ~myASTVisitor() {
        
    }
    
    virtual bool VisitExpr(Expr *expr) {
               numExprs++;
            QualType q1 = expr->getType();
            expr = expr->IgnoreConversionOperator();
            expr = expr->IgnoreImpCasts();

            QualType q2 = expr->getType();
            
            string type1 = q1.getAsString();
            string type2 = q2.getAsString();

            //errs()<<"\n"<<type2<<"-->"<<type1;
            string booltype = "_Bool";
            
            if( (type1==booltype) && (type2.find("Vc")!=string::npos) && (type2.find("Mask")!=string::npos) )
            {
                errs()<<"\n";
                SourceLocation loc = expr->getExprLoc();
                loc.dump(astContext->getSourceManager());
                errs() << ": Found an implicit conversion from VcMask to Bool. ";
            }
        return true;
    }
};



class myASTConsumer : public ASTConsumer {
private:
    myASTVisitor *visitor;
    
public:
    explicit myASTConsumer(CompilerInstance *CI) : visitor(new myASTVisitor(CI)) {
        
    } //override the constructor to pass CI
    
    //override the call so our AST Visitor can visit the entire source file
    virtual void HandleTranslationUnit(ASTContext &Context) {
        //get TranslationUnitDecl - Collectively entire source file
        visitor->TraverseDecl(Context.getTranslationUnitDecl());
    }
};

class myFrontendAction : public ASTFrontendAction {
public:
    virtual std::unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &CI, StringRef file) {
        return std::unique_ptr<ASTConsumer>(new myASTConsumer(&CI));
    }
};



int main(int argc, const char **argv) {
    // parse the command line arguments passed to your code
    CommonOptionsParser op(argc,argv,ClangCheckCategory);
    // create a new ClangTool instance (a LibTooling Environment)
    ClangTool Tool(op.getCompilations(), op.getSourcePathList());
    
    // run the Clang Tool, creating a new FrontendAction
    int result = Tool.run(newFrontendActionFactory<myFrontendAction>().get());
    
    errs() << "\nFound "<<numExprs<<" Exprs.\n\n";
    
    
    return result;
}